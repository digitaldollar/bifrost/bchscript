module gitlab.com/digitaldollar/bifrost/bchscript

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/gcash/bchd v0.17.1
	github.com/gcash/bchlog v0.0.0-20180913005452-b4f036f92fa6
	github.com/gcash/bchutil v0.0.0-20201025062739-fc759989ee3e
	golang.org/x/crypto v0.0.0-20201208171446-5f87f3452ae9
)
